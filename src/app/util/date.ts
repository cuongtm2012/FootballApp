export default class DateUtil {
    static getDateCurrent(date) {
        let day, month, year;
        year = date.substr(0, 4);
        month = date.substr(5, 2);
        day = date.substr(8, 2);
        return year + '-' + month + '-' + day;
    }

    static getNextDay(date) {
        let day, month, year;
        year = parseInt(date.substr(0, 4), 0);
        month = parseInt(date.substr(5, 2), 0);
        day = parseInt(date.substr(8, 2), 0);

        if ((day === 30 && month === 4) || (day === 30 && month === 6) || (day === 30 && month === 9) || (day === 30 && month === 11)) {
            day = 1;
            month = month + 1;
            year = year;
        } else if ((day === 31 && month === 1) || (day === 31 && month === 3)
            || (day === 31 && month === 5) || (day === 31 && month === 7)
            || (day === 31 && month === 7) || (day === 31 && month === 8)
            || (day === 31 && month === 10)) {
            day = 1;
            month = month + 1;
            year = year;
        } else if (year % 4 === 0 && month === 2 && day === 29) {
            day = 1;
            month = month + 1;
            year = year;
        } else if (year % 4 !== 0 && month === 2 && day === 28) {
            day = 1;
            month = month + 1;
            year = year;
        } else if (day === 31 && month === 12) {
            day = 1;
            month = 1;
            year = year + 1;
        } else {
            day = day + 1;
            month = month;
            year = year;
        }

        if (day < 10) {
            day = '0' + day.toString();
        } else {
            day = day.toString();
        }

        if (month < 10) {
            month = '0' + month.toString();
        } else {
            month = month.toString();
        }
        return year.toString() + '-' + month.toString() + '-' + day.toString();
    }

    static getPreviousDay(date) {
        let day, month, year;
        year = parseInt(date.substr(0, 4), 0);
        month = parseInt(date.substr(5, 2), 0);
        day = parseInt(date.substr(8, 2), 0);

        if (month === 2 && day === 1 || month === 4 && day === 1 || month === 6 && day === 1
            || month === 8 && day === 1 || month === 9 && day === 1 || month === 11 && day === 1) {
            day = 31;
            month = month - 1;
            year = year;
        } else if (month === 5 && day === 1 || month === 7 && day === 1
            || month === 10 && day === 1 || month === 12 && day === 1) {
            day = 30;
            month = month - 1;
            year = year;
        } else if (month === 1 && day === 1) {
            day = 31;
            month = 12;
            year = year - 1;
        } else if (month === 3 && year % 4 === 0 && day === 1) {
            day = 29;
            month = month - 1;
            year = year;
        } else if (month === 3 && year % 4 !== 0 && day === 1) {
            day = 28;
            month = month - 1;
            year = year;
        } else {
            day = day - 1;
            month = month;
            year = year;
        }

        if (day < 10) {
            day = '0' + day.toString();
        } else {
            day = day.toString();
        }

        if (month < 10) {
            month = '0' + month.toString();
        } else {
            month = month.toString();
        }

        return year.toString() + '-' + month.toString() + '-' + day.toString();
    }
}

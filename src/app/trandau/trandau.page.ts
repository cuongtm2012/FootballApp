import { Component, OnInit } from '@angular/core';
import { EnvService } from '../service/env.service';
import { RequestService } from '../service/request.service';
import DateUtil from '../util/date';
import { DatePipe } from '@angular/common';
@Component({
  selector: 'app-trandau',
  templateUrl: './trandau.page.html',
  styleUrls: ['./trandau.page.scss']
})
export class TrandauPage implements OnInit {
  eventSwipe: boolean;
  arrayTaiSuus = [];
  currentDay: any;
  date = new Date();
  dateCurrent: string;
  isLoading: boolean;
  isError: boolean;
  valueSearch: any;
  textSearch: string;
  constructor(
    private envService: EnvService,
    private datePipe: DatePipe,
    private requestService: RequestService
  ) {
  }

  ionViewWillEnter(): void {
    this.isLoading = true;
    this.isError = false;
    this.dateCurrent = this.datePipe.transform(this.date, 'yyyy-MM-dd');
    this.reloadDataTaiSuu(this.dateCurrent);
  }

  reloadDataTaiSuu(date) {
    this.arrayTaiSuus = [];
    this.loadDataTaiSuu(date);
  }

  loadDataTaiSuu(date) {
    const urlLoadDataTaiSuu = this.envService.API_URL + this.envService.URL_LOAD_DATA_TAI_SUU;
    const params = [];
    params.push({ key: 'date', value: date });
    this.requestService.get(urlLoadDataTaiSuu, params, undefined,
      (response) => this.onSuccessLoadDataTaiSuu(response),
      (error) => this.onErrorLoadDataTaiSuu(error),
      () => { });

  }

  onErrorLoadDataTaiSuu(error) {
    console.log(error);
    this.isError = true;
  }

  onSuccessLoadDataTaiSuu(response) {
    console.log(response);
    this.isLoading = false;
    for (let index = 0; index < Object.keys(response).length; index++) {
      this.arrayTaiSuus.push(response[index]);
    }
  }

  ngOnInit() {
  }

  previousDay() {
    this.isLoading = true;
    this.textSearch = '';
    this.dateCurrent = DateUtil.getPreviousDay(this.dateCurrent);
    this.reloadDataTaiSuu(this.dateCurrent);
  }

  nextDay() {
    this.isLoading = true;
    this.textSearch = '';
    this.reloadDataTaiSuu(this.dateCurrent);
    this.dateCurrent = DateUtil.getNextDay(this.dateCurrent);
  }

  onRetry() {
    this.isLoading = true;
    this.isError = false;
    this.reloadDataTaiSuu(this.dateCurrent);
  }

  onSwipe(evt) {
    // this.eventSwipe = (evt.deltaX > 0 ? 'right' : 'left') === 'left';
    // this.isLoading = true;
    // if (this.eventSwipe === true) {
    //   this.dateCurrent = DateUtil.getPreviousDay(this.dateCurrent);
    //   this.reloadDataTaiSuu(this.dateCurrent);
    // } else if (this.eventSwipe === false) {
    //   this.dateCurrent = DateUtil.getNextDay(this.dateCurrent);
    //   this.reloadDataTaiSuu(this.dateCurrent);
    // }
  }

  onInputSearch(event) {
    this.valueSearch = event.target.value;
  }
}
